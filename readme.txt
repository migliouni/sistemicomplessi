Dopo aver installato Cromosim per Python3 tramite pip3, oppure dalla relativa pagina git:

https://github.com/sylvain-faure/cromosim/


Sostituire i file nella cartella dell'istallazione di Cromosim (solitamente situata in Python\Python36\site-packages\cromosim), con i file presenti nella cartella "FileSostituireCromosim"


Per eseguire una simulazione spostarsi nella cartella del modello desiderato e lanciare il comando:

python NOME_FILE_MODELLO.py --json NOME_FILE_INPUT.json


Per esempio per il modello CA: 

python cellular_automata.py --json input.json